@extends('layouts.app')

@section('title', ' | My Products')

@section('content')
    <div class="d-flex w-100 justify-content-between">
        <h1>My Products</h1>
        <div>
            <p class="my-product-cnt"><span class="my-product-cnt-fc">Current Plan: </span><span class="my-product-cnt-lc">{{$counter['current_plan']}}</span></p>
            <p class="my-product-cnt"><span class="my-product-cnt-fc">Product Imported this Month: </span><span class="my-product-cnt-lc">{{$counter['total_product']}}</span></p>
            <p class="my-product-cnt"><span class="my-product-cnt-fc">Affiliate Product Imported this Month: </span><span class="my-product-cnt-lc">{{$counter['total_affiliate_product']}}</span></p>
        </div>
    </div>
    <products-manager></products-manager>
@endsection
