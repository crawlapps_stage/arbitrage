<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show custom-sdbar" id="sidebar">
    <div class="logo-font">
        <span>AmaZone</span>
        <span class="sub-lg">DropShipper</span>
        {{--        <img class="c-sidebar-brand-full" src="/images/amazone-dropshipper-logo.png"--}}
        {{--            alt="AmaZone Dropshipper Logo" style="width: 80%; height: auto;"><img class="c-sidebar-brand-minimized"--}}
        {{--            src="/images/amazone-dropshipper-logo.png" alt="AmaZone Dropshipper Logo" style="width: 80%; height: auto;">--}}
    </div>
    <ul class="c-sidebar-nav mt-3">
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link{{ request()->is('/') ? ' c-active' : '' }}" href="/">
{{--                <i class="c-sidebar-nav-icon fas fa-star"></i> --}}
                Import Products
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link{{ request()->is('/products') ? ' c-active' : '' }}" href="/products">
{{--                <i class="c-sidebar-nav-icon fa fa-list-ul"></i> --}}
                My Products
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link{{ request()->is('/orders') ? ' c-active' : '' }}" href="/orders">
{{--                <i class="c-sidebar-nav-icon fas fa-tags"></i> --}}
                My Orders
            </a>
        </li>

{{--         hide help child menu and show only parent--}}
        <li class="c-sidebar-nav-item mt-auto">
            <a class="c-sidebar-nav-link{{ request()->is('/help') ? ' c-active' : '' }}" href="/help">
{{--                <i class="c-sidebar-nav-icon fas fa-tags"></i>--}}
                Help Topics
            </a>
        </li>

{{--        <li class="c-sidebar-nav-dropdown mt-auto"><a class="c-sidebar-nav-dropdown-toggle{{ request()->is('/help*') ? ' c-active' : '' }}" href="#"><i class="c-sidebar-nav-icon fas fa-question-circle"></i> Help Center</a>--}}
{{--            <ul class="c-sidebar-nav-dropdown-items">--}}
{{--                <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link{{ request()->is('/help') ? ' c-active' : '' }}" href="/help">Help Topics</a></li>--}}
{{--                <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link{{ request()->is('/help/tutorials') ? ' c-active' : '' }}" href="/help/tutorials">Tutorials</a></li>--}}
{{--                <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link{{ request()->is('/help/contact') ? ' c-active' : '' }}" href="/help/contact">Support &amp; Feedback</a></li>--}}
{{--                <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link{{ request()->is('/help/announcements') ? ' c-active' : '' }}" href="/help/announcements">Announcements</a></li>--}}
{{--            </ul>--}}
{{--        </li>--}}
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link{{ request()->is('/settings') ? ' c-active' : '' }}" href="/settings">
{{--                <i class="c-sidebar-nav-icon fas fa-tags"></i> --}}
                Settings
            </a>
        </li>
{{--        <li class="c-sidebar-nav-item mb-3">--}}
{{--            <a class="c-sidebar-nav-link" href="https://{{ Auth::user()->getDomain()->toNative() }}">--}}
{{--                <i class="c-sidebar-nav-icon fas fa-shopping-bag"></i> My Shop--}}
{{--            </a>--}}
{{--        </li>--}}
    </ul>
</div>
