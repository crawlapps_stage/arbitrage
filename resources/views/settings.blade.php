@extends('layouts.app')

@section('title', ' | Settings')

@section('content')
<h1>Settings</h1>
<settings-manager :prop-plan="{{ $settings->plan_id }}" prop-amazon-associate-btn="{{ $settings->amazon_associate_btn }}" :prop-diagnostics="Boolean({{ $settings->diagnostics }})"></settings-manager>
@endsection