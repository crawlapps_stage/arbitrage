const host = process.env.MIX_APP_URL;
const apiEndPoint = host + '/api';

if(!window.jQuery)
{
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
    document.getElementsByTagName('head')[0].appendChild(script);
}
window.Vue = require('vue');
import axios from "axios";

const app = new Vue({
        template: '<div></div>',
        data: {
            shopifyDomain: '',
        },
        methods: {
            init() {
                let url = window.location.href;

                if (url.includes("/")) {
                    let params = this.getParams('amazon-dropshipper');
                    this.shopifyDomain = params['shop'];
                    let handle = url.split('/').pop();
                    if (url.includes("/products/")) {
                        this.checkAffiliate(handle);
                    }
                }

            },
            checkAffiliate(handle) {
                let base = this;
                let aPIEndPoint = `${apiEndPoint}/check-affiliate?shop=${base.shopifyDomain}&handle=` + handle;
                axios.get(aPIEndPoint)
                    .then(res => {
                        let data = res.data.data;
                        if(data.is_product){
                            let anode = document.createElement("a");
                            if( data.style === 0 ){
                                anode.innerHTML = data.show_data;
                            }else if( data.style === "1" || data.style === "2" ){
                                let imgd = document.createElement('img');
                                imgd.src = host + data.show_data;
                                imgd.style = "width:150px;";
                                anode.appendChild(imgd);
                            }
                            anode.href = data.redirect;
                            anode.target = '_blank';
                            let cart = document.getElementsByName('add');
                            if( cart.length >= 1 ){
                                if(data.style === 0){
                                    let cartClass = cart[0].className;
                                    anode.className = cartClass;
                                    cart[0].replaceWith(anode);
                                    // cart[0].replaceChild(anode, cart[0].childNodes[1]);
                                }else{
                                    let parent = cart[0].parentNode;
                                    parent.replaceChild(anode, parent.childNodes[1]);
                                }
                            }else{
                                let frms = document.getElementsByTagName('form');
                                for (var i = 0; i < frms.length; i++) {
                                    let ac = 'https://' + base.shopifyDomain + '/cart/add';
                                    if(frms[i].action === ac){
                                        let btns = $(frms[i]).find('button');
                                        for (var i = 0; i < btns.length; i++) {
                                            if( btns[i].type === 'submit' ){
                                                if(data.style === 0){
                                                    let btnClass = btns[i].className;
                                                    anode.className = btnClass;
                                                    btns[i].replaceWith(anode);
                                                    // btns[i].replaceChild(anode, btns[i].childNodes[1]);
                                                }else{
                                                    $( btns[i] ).replaceWith(anode );
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });
            },
            getParams(script_name) {
                // Find all script tags
                var scripts = document.getElementsByTagName("script");
                // Look through them trying to find ourselves
                for (var i = 0; i < scripts.length; i++) {
                    if (scripts[i].src.indexOf("/" + script_name) > -1) {
                        // Get an array of key=value strings of params
                        var pa = scripts[i].src.split("?").pop().split("&");
                        // Split each key=value into array, the construct js object
                        var p = {};
                        for (var j = 0; j < pa.length; j++) {
                            var kv = pa[j].split("=");
                            p[kv[0]] = kv[1];
                        }
                        return p;
                    }
                }

                // No scripts match

                return {};
            }
        },
        created() {
            this.init();
        }
        ,
    })
;

Window.crawlapps_order_delivery = {
    init: function () {
        app.$mount();
    },
};

window.onload = Window.crawlapps_order_delivery.init();
