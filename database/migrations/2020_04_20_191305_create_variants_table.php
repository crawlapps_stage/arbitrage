<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('image_id')->unsigned();
            $table->boolean('is_main')->default(0);
            $table->string('title');
            $table->boolean('in_stock')->default(1);
            $table->integer('inventory')->unsigned()->default(0);
            $table->boolean('prime_eligible')->default(0);
            $table->string('shipping_type', 150)->default('Standard Shipping');
            $table->string('source_id', 500);
            $table->decimal('source_price', 8, 2)->default('0.00');
            $table->decimal('shipping_price', 8, 2)->default('0.00');
            $table->decimal('shopify_price', 8, 2)->default('0.00');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
