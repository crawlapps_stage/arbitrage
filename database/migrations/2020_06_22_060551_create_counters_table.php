<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->unsigned();
            $table->integer('charge_id');
            $table->integer('plan_id');
            $table->integer('regular_product_count')->default(0);
            $table->integer('regular_product_variant_count')->default(0);
            $table->integer('affiliate_product_count')->default(0);
            $table->integer('affiliate_product_variant_count')->default(0);
            $table->string('status')->nullable();
            $table->boolean('is_disable_freemium')->default(1);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->timestamps();

//            $table->foreign('charge_id')->references('id')->on('charges')->onDelete('CASCADE')->onUpdate('NO ACTION');
//            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('CASCADE')->onUpdate('NO ACTION');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counters');
    }
}
