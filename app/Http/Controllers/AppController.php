<?php

namespace App\Http\Controllers;

use App\Jobs\AddProductImagesJob;
use App\Models\Counters;
use Keepa\API\Request as KeepaAPIRequest;
use Keepa\API\ResponseStatus;
use Keepa\helper\CSVType;
use Keepa\helper\CSVTypeWrapper;
use Keepa\helper\KeepaTime;
use Keepa\helper\ProductAnalyzer;
use Keepa\helper\ProductType;
use Keepa\KeepaAPI;
use Keepa\objects\AmazonLocale;
use Symfony\Component\Intl\Currencies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AppController extends Controller
{
    /**
     * Show Import Products page
     *
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        return view('import');
    }

    /**
     * Get Amazon product info
     *
     * @return \Illuminate\Http\Response
     */
    public function getAmazonProduct(Request $request)
    {
        try {
            $to_currency = $this->getShopCurrency();
            $from_currency = $request['currency'];

            $errors = [];
            $diagnostics = [];
            // Initialize product info array
            $productInfo = [
                'collection' => 0,
                'type' => '',
                'title' => '',
                'description' => '',
                'prime_eligible' => false,
                'in_stock' => false,
                'shipping_type' => 'Standard shipping',
                'source' => 'Amazon',
                'source_url' => '',
                'source_id' => '',
                'locale' => '',
                'source_price' => 0.00,
                'shopify_price' => 0.00,
                'shipping_price' => 0.00,
                'images' => [],
                'main_image' => 0,
                'inventory' => 10,
                'variants' => [],
                'attributes' => [],
                'tags' => '',
                'currency' => '',
                'currency_symbol' => Currencies::getSymbol($to_currency),
                'review_count' => '',
                'saller_ranks' => [],
            ];

            // Initialize variant info arrays
            $apiVariants = [];
            $variantAttributes = [];

            // Check locale
            $locale = strtoupper($request->post('locale'));

            if (!preg_match('~^[A-Z]{2}$~', $locale) || ($locale != 'BR' && !defined("Keepa\objects\AmazonLocale::".$locale))) {
                $errors[] = 'Invalid Amazon locale.';
            }

            $productInfo['locale'] = $locale;

            // Check Product ID (ASIN)
            $id = strtoupper($request->post('id'));

            if (!preg_match('~^[a-zA-Z0-9]{10}$~', $id)) {
                $errors[] = 'Invalid Amazon product identifier (ASIN).';
            }

            if ($errors) {
                return ['success' => false, 'errors' => $errors, 'product' => $productInfo];
            }

            $productInfo['source_id'] = $id;

            $includeVariants = $request->post('include_variants') ? true : false;

            /* Fetch product info from API
             *
             *  $domainID int Amazon locale of the product: AmazonLocale::US = 1     AmazonLocale::GB = 2    AmazonLocale::DE = 3
             *                                              AmazonLocale::FR = 4     AmazonLocale::JP = 5    AmazonLocale::CA = 6
             *                                              AmazonLocale::CN = 7     AmazonLocale::IT = 8    AmazonLocale::ES = 9
             *                                              AmazonLocale::IN = 10    AmazonLocale::MX = 11   AmazonLocale::AU = 12
             *                                              AmazonLocale::BR = 13 *BR is not officially documented yet but appears to be working
             *
             *  $offers int If specified (= not null) Determines the number of marketplace offers to retrieve. <b>Not available for Amazon China.</b>
             *
             *  $statsStartDate string Must ISO8601 coded date (with or without time in UTC). Example: 2015-12-31 or 2015-12-31T14:51Z.
             *                  If specified (= not null) the product object will have a stats field with quick access to current prices,
             *                  min/max prices and the weighted mean values in the interval specified statsStartDate to statsEndDate.
             *
             *  $statsEndDate string the end of the stats interval. See statsStartDate.
             *
             *  $update int If the product's last refresh is older than <i>update</i>-hours force a refresh.
             *              Use this to speed up requests if up-to-date data is not required. Might cost an extra token if 0 (= live data). Default 1.
             *
             *  $history bool Whether or not to include the product's history data (csv field). If you do not evaluate the csv field set to false to speed up the request and reduce traffic.
             *
             *  $asins string[] ASINs to request, must contain between 1 and 100 ASINs - or max 20 ASINs if the offers parameter is used.
             */

            $api = new KeepaAPI(config('const.keepa_api_key'));

            $r = KeepaAPIRequest::getProductRequest(constant("Keepa\objects\AmazonLocale::$locale"), 0, null, null, 0,
                true,
                [$id]);
            $response = $api->sendRequestWithRetry($r);

            switch ($response->status) {

                case ResponseStatus::OK:

                    // Check that at least one non-empty product is returned
                    if (isset($response->products) && is_array($response->products) && count($response->products) > 0 &&
                        isset($response->products[0]->title) && $response->products[0]->title) {

                        $product = $response->products[0];
                        if ($product->productType == ProductType::STANDARD || $product->productType == ProductType::DOWNLOADABLE) {

                            $productInfo['title'] = $product->title;
                            $productInfo['description'] = $product->description;

                            if ($product->features) {
                                $features = '';

                                foreach ($product->features as $feature) {
                                    $features .= "<li>$feature</li>\n";
                                }

                                $productInfo['description'] = "<ul>\n$features\n</ul>\n".$productInfo['description'];
                            }
                            $productInfo['prime_eligible'] = $product->isEligibleForSuperSaverShipping;

                            $productInfo['review_count'] = ProductAnalyzer::getLast($product->csv[CSVType::RATING], CSVTypeWrapper::getCSVTypeFromIndex(CSVType::RATING));

                            $currentAmazonPrice = ProductAnalyzer::getLast($product->csv[CSVType::AMAZON], CSVTypeWrapper::getCSVTypeFromIndex(CSVType::AMAZON));

                            if ($currentAmazonPrice == -1) {
                                $currentAmazonPrice = ProductAnalyzer::getLast($product->csv[CSVType::MARKET_NEW],
                                    CSVTypeWrapper::getCSVTypeFromIndex(CSVType::MARKET_NEW));
                            }

                            if ($currentAmazonPrice != -1) {
                                $productInfo['in_stock'] = true;
                                $price = number_format($currentAmazonPrice / 100, 2, '.', '');
                                $productInfo['source_price'] =  $this->calculateCurrency($from_currency, $to_currency, $price);
                                $productInfo['shopify_price'] = $productInfo['source_price'];
                            }
                            if ($product->imagesCSV) {
                                $imgArray = explode(',', $product->imagesCSV);
                                foreach ($imgArray as $img) {
                                    $productInfo['images'][] = [
                                        'src' => config('const.amazon_images_url').$img, 'variant' => -1,
                                        'selected' => true
                                    ];
                                }
                            }
                            // Add variants [extra call to API]
                            if ($includeVariants && $product->variations) {
                                foreach ($product->variations as $variant) {
                                    $apiVariants[] = $variant->asin;
                                    $variantAttributes[$variant->asin] = $variant->attributes;
                                }
                            }

                            // Set product attributes
                            if (isset($variantAttributes[$productInfo['source_id']])) {
                                $productInfo['attributes'] = $variantAttributes[$productInfo['source_id']];
                            }

                            // saller ranks
                            $salesRank = $product->salesRanks;
                            foreach( $salesRank as $key=>$val ){
                                if( end($val) != -1 ){
                                    $categoryTree = $product->categoryTree;
                                    foreach( $categoryTree as $ckey=>$cval ){
                                        $productInfo['saller_ranks'][$key]['name'] = ( $cval->catId == $key ) ? $cval->name : 'Shops';
                                    }
                                    $productInfo['saller_ranks'][$key]['count'] = end($val);
                                }
                            }
                        }
                    } else {
                        $errors[] = 'Could not fetch product. Please double-check your entry or try a different URL.';
                    }

                    break;

                default:
                    if ($response->error && $response->error->message) {
                        $errors[] = $response->error->message;
                    }
            }
            // Remove main variant from further API calls
//            if (($key = array_search($productInfo['source_id'], $apiVariants)) !== false) {
//                unset($apiVariants[$key]);
//            }

            // Make API call to retrieve product variants
            // TODO: Re-factor to use single API call function for main product & variants
            if (!count($errors) && count($apiVariants)) {

                $r = KeepaAPIRequest::getProductRequest(constant("Keepa\objects\AmazonLocale::$locale"), 0, null, null,
                    0,
                    true, $apiVariants);

                $response = $api->sendRequestWithRetry($r);

                switch ($response->status) {

                    case ResponseStatus::OK:

                        // Check that at least one non-empty product is returned
                        if (isset($response->products) && is_array($response->products) && count($response->products) > 0 &&
                            isset($response->products[0]->title) && $response->products[0]->title) {

                            for ($i = 0; $i < min(config('const.max_variants_num'), count($response->products)); $i++) {

                                $productInfo['variants'][$i] = [
                                    'checked' => true,
                                    'title' => '',
                                    'in_stock' => false,
                                    'shipping_type' => 'Standard shipping',
                                    'source_id' => '',
                                    'source_price' => 0.00,
                                    'shopify_price' => 0.00,
                                    'shipping_price' => 0.00,
                                    'main_image' => 0,
                                    'inventory' => 10
                                ];

                                $product = $response->products[$i];

                                $productInfo['variants'][$i]['source_id'] = $product->asin;
                                $productInfo['variants'][$i]['title'] = $product->title;

                                $productInfo['variants'][$i]['prime_eligible'] = $product->isEligibleForSuperSaverShipping;

                                $currentAmazonPrice = ProductAnalyzer::getLast($product->csv[CSVType::AMAZON],
                                    CSVTypeWrapper::getCSVTypeFromIndex(CSVType::AMAZON));
                                if ($currentAmazonPrice == -1) {
                                    $currentAmazonPrice = ProductAnalyzer::getLast($product->csv[CSVType::MARKET_NEW],
                                        CSVTypeWrapper::getCSVTypeFromIndex(CSVType::MARKET_NEW));
                                }
//                                dd($currentAmazonPrice);
                                if ($currentAmazonPrice != -1) {
                                    $productInfo['variants'][$i]['in_stock'] = true;
                                    $price = number_format($currentAmazonPrice / 100,
                                        2, '.', '');

                                    $productInfo['variants'][$i]['source_price'] = $this->calculateCurrency($from_currency, $to_currency, $price);
                                    $productInfo['variants'][$i]['shopify_price'] = $productInfo['variants'][$i]['source_price'];
                                }
                                if ($product->imagesCSV) {

                                    $imgArray = explode(',', $product->imagesCSV);

                                    for ($j = 0; $j < count($imgArray); $j++) {
                                        $productInfo['images'][] = [
                                            'src' => config('const.amazon_images_url').$imgArray[$j], 'variant' => $i,
                                            'selected' => true
                                        ];
                                    }
//                                    $productInfo['variants'][$i]['main_image'] = count($productInfo['images']) - $j + 1;
                                    $mi = count($productInfo['images']) - $j + 1;
                                    $productInfo['variants'][$i]['main_image'] = ( $mi <= count($productInfo['images']) - 1  )? $mi : count($productInfo['images']) - 1;
                                }
                                //                            if ($product->imagesCSV) {
                                //                                $imgArray = explode(',', $product->imagesCSV);
                                //                                $main_image = '';
                                //
                                //                                $f = 0;
                                //                                for ($j = 0; $j < count($imgArray); $j++) {
                                //                                    // START :: remove duplication of image
                                //                                    $srcs = [];
                                //                                    foreach ($productInfo['images'] as $key => $val) {
                                //                                        $srcs[] = $val['src'];
                                //                                    }
                                //                                    $img = config('const.amazon_images_url').$imgArray[$j];
                                //                                    if (in_array($img, $srcs)) {
                                //                                        $variant = array_search($img, $srcs);
                                //                                        if ($j == 0) {
                                //                                            $main_image = $variant;
                                //                                        }
                                //                                    } else {
                                //                                        $f++;
                                //                                        $variant = '';
                                //                                        $productInfo['images'][] = [
                                //                                            'src' => $img, 'variant' => $i,
                                //                                            'selected' => true
                                //                                        ];
                                //                                    }
                                //                                    // END :: remove duplication of image
                                ////                                    $productInfo['images'][] = ['src' => config('const.amazon_images_url') . $imgArray[$j], 'variant' => $i, 'selected' => true];
                                //                                }
                                //                                if ($main_image == '') {
                                //                                    if ($f > 0) {
                                //                                        $productInfo['variants'][$i]['main_image'] = count($productInfo['images']) - $f + 1;
                                //                                    }
                                //                                } else {
                                //                                    $productInfo['variants'][$i]['main_image'] = ($main_image) ? $main_image : $variant;
                                //                                }
                                ////                                $productInfo['variants'][$i]['main_image'] = count($productInfo['images']) - $j + 1;
                                //                            }

                                // Add variant attributes
                                if (isset($variantAttributes[$product->asin])) {
                                    $productInfo['variants'][$i]['attributes'] = $variantAttributes[$product->asin];
                                }
                            }
                        }

                    default:
                        if ($response->error && $response->error->message) {
                            $errors[] = $response->error->message;
                        }
                }
            }


            // remove duplicate images
            foreach ($productInfo['images'] as $key => $val) {
                $srcs[] = $val['src'];
            }

            if( !empty($srcs) ){
                $u_images = array_unique($srcs);
                foreach ($u_images as $key => $val) {
                    $productInfo['unique_images'][] = ['src' => $val, 'selected' => true];
                }
            }

             // group by first attributes
            if( !empty( $productInfo['variants'] ) ){
                $productInfo['variants'] = $this->group_by($productInfo['variants']);
            }

            // Diagnostics
            $diagnostics[] = ['name' => 'Request time (s)', 'value' => $response->requestTime / 1000];
            $diagnostics[] = ['name' => 'Processing time (s)', 'value' => $response->processingTimeInMs / 1000];
            $diagnostics[] = ['name' => 'Tokens consumed', 'value' => $response->tokensConsumed];
            $diagnostics[] = ['name' => 'Tokens left', 'value' => $response->tokensLeft];
            $diagnostics[] = ['name' => 'Tokens refill in (s)', 'value' => $response->refillIn / 1000];
            $diagnostics[] = ['name' => 'Tokens refill rate', 'value' => $response->refillRate];
            $diagnostics[] = ['name' => 'Tokens flow reduction', 'value' => $response->tokenFlowReduction];

            return ['errors' => $errors, 'diagnostics' => $diagnostics, 'product' => $productInfo];
        }catch(\Exception $e){
            dd($e);
            return ['success' => false, 'errors' => $errors, 'product' => $productInfo];
//            return false;
        }
    }
    public function getShopCurrency(){
        $shop = Auth::user();
        $parameter['fields'] = 'currency';
        $shop_result = $shop->api()->rest('GET', 'admin/api/' . env('SHOPIFY_API_VERSION') . '/shop.json', $parameter);

        if( !$shop_result->errors ){
            return $shop_result->body->shop->currency;
        }
    }
    //currency converter
    public function calculateCurrency($fromCurrency, $toCurrency, $amount)
    {
        try{
            $amount = urlencode($amount);
            $fromCurrency = urlencode($fromCurrency);
            $toCurrency = urlencode($toCurrency);
            $rawdata = file_get_contents("https://data.fixer.io/api/convert?access_key=3175c637bf1d91ef7dedd6640654bb1a&from=$fromCurrency&to=$toCurrency&amount=$amount&format=1");

            $data = json_decode($rawdata, true);
            return round($data['result'], 2);
        }catch(\Exception $e){
            dd($e);
        }

    }

    public function group_by($data) {
       try{
           foreach( $data as $key=>$val){
               if( !empty($val['attributes']) ){
                    $dimval[] = (is_array($val['attributes'][0])) ? $val['attributes'][0]['value'] : $val['attributes'][0]->value;
               }
           }
           $newDimVal = array_unique($dimval);
           foreach( $newDimVal as $key=>$val ){
               foreach( $data as $k=>$v ){
                   if(is_array($v['attributes'][0])){
                       if( $v['attributes'][0]['value'] == $val){
                           $newProductVariants[] = $v;
                       }
                   }else{
                       if( $v['attributes'][0]->value == $val){
                           $newProductVariants[] = $v;
                       }
                   }

               }
           }
           return $newProductVariants;
       }catch (\Exception $e){
           dd($e);
       }
    }
    /**
     * Get Walmart US product info
     *
     * @return \Illuminate\Http\Response
     */
    public function getWalmartProduct(Request $request)
    {
        $to_currency = $this->getShopCurrency();
        $from_currency = $request['currency'];

        $errors = [];
        $diagnostics = [];

        // Initialize product info array
        $productInfo = [
            'collection' => 0,
            'type' => '',
            'title' => '',
            'description' => '',
            'prime_eligible' => false,
            'in_stock' => false,
            'shipping_type' => 'Standard shipping',
            'source' => 'Walmart',
            'source_url' => '',
            'source_id' => '',
            'locale' => 'US',
            'source_price' => 0.00,
            'shopify_price' => 0.00,
            'shipping_price' => 0.00,
            'images' => [],
            'main_image' => 0,
            'inventory' => 10,
            'variants' => [],
            'attributes' => [],
            'tags' => '',
            'currency' => '',
            'currency_symbol' => Currencies::getSymbol($to_currency),
        ];

        // Check Walmart Product ID
        $productID = $request->post('product_id');

        if (!preg_match('~^[0-9]{5,12}$~', $productID)) {
            $errors[] = 'Invalid Walmart product id.';
        }

        if ($errors) {
            return ['success' => false, 'errors' => $errors, 'product' => $productInfo];
        }

        $apiURL = config('const.walmart_api_url').$productID.'?apiKey='.config('const.walmart_api_key');

        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $apiURL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT,
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36');
            $data = curl_exec($curl);

            if (curl_errno($curl)) {
                throw new Exception(curl_error($curl));
            }
            curl_close($curl);
            $data = json_decode($data, true);

            if (isset($data['errors'])) {
                throw new Exception('Could not find product in Walmart database.');
            }
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
        }

        $productInfo['source_id'] = $productID;

        $productInfo['title'] = isset($data['name']) ? $data['name'] : '';
        $productInfo['description'] = isset($data['shortDescription']) ? $data['shortDescription'] : '';

        if (isset($data['longDescription'])) {
            $productInfo['description'] .= "<br>\n".$data['longDescription'];
        }

        if (isset($data['stock']) && $data['stock'] == 'Available') {
            $productInfo['in_stock'] = true;
        }

        if (isset($data['salePrice'])) {
            $productInfo['source_price'] = floatval($data['salePrice']);
        }

        if (isset($data['standardShipRate'])) {
            $productInfo['source_price'] += floatval($data['standardShipRate']);
            $productInfo['shipping_price'] = floatval($data['standardShipRate']);
        }

        $price = number_format($productInfo['source_price'], 2, '.', '');

        $productInfo['source_price'] = $this->calculateCurrency($from_currency, $to_currency, $price);
        $productInfo['shopify_price'] = $productInfo['source_price'];

        if (isset($data['isTwoDayShippingEligible']) && $data['isTwoDayShippingEligible'] == true) {
            $productInfo['shipping_type'] = "Two days shipping";
        } else {
            if (isset($data['ninetySevenCentShipping']) && $data['ninetySevenCentShipping'] == true) {
                $productInfo['shipping_type'] = "Ninetyseven cent shipping";
            }
        }

        // Product images
        if (isset($data['imageEntities'])) {
            foreach ($data['imageEntities'] as $img) {
                $productInfo['images'][] = ['src' => $img['largeImage'], 'variant' => -1, 'selected' => true];
            }
        }

        // Variants
        $variantAttributes = [];

        $skipAttrs = [
            'productUrlText', 'wklyFcstWeeks', 'isPvtLabelUnbranded', 'uniqueProductId', 'actualColor',
            'productUrlText',
            'finerCateg', 'compositeWood', 'isPvtLabelUnbranded', 'replenishmentStartDate', 'isImport', 'mainimageurl',
            'batteryType', 'isSortable', 'ppuQty', 'paperWoodIndicator', 'ppuQtyUom', 'prodClassType',
            'ironBankCategory',
            'canonicalUrl', 'gender', 'isOrderable', 'fuelRestriction', 'clothingSize',
            'caResidentsPropWarningRequired',
            'hasWarranty', 'mirrorNum', 'replenishmentEndDate', 'pesticideInd', 'assemblyRequiredInd', 'replenType',
            'requiresTextileActLabeling', 'karfIsAlcohol', 'globalProductType', 'globalProductTypeCharpath',
            'ConsumerItemNumber'
        ];

        foreach ($data['attributes'] as $key => $value) {
            if (!in_array($key, $skipAttrs)) {
                $variantAttributes[] = $key;

                // Set product attributes
                $productInfo['attributes'][] = ['dimension' => ucfirst($key), 'value' => ucfirst($value)];
            }
        }

        if (isset($data['variants'])) {

            for ($i = 0; $i < min(config('const.max_variants_num'), count($data['variants'])); $i++) {

                $productInfo['variants'][$i] = [
                    'checked' => true,
                    'source_id' => $data['variants'][$i],
                    'title' => '',
                    'shipping_type' => 'Standard shipping',
                    'in_stock' => false,
                    'source_price' => 0.00,
                    'shopify_price' => 0.00,
                    'shipping_price' => 0.00,
                    'images' => [],
                    'main_image' => 0,
                    'inventory' => 10,
                    'attributes' => []
                ];

                $apiURL = config('const.walmart_api_url').$data['variants'][$i].'?apiKey='.config('const.walmart_api_key');

                try {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
                    curl_setopt($curl, CURLOPT_HEADER, false);
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl, CURLOPT_URL, $apiURL);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_USERAGENT,
                        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36');
                    $variantData = curl_exec($curl);

                    if (curl_errno($curl)) {
                        throw new Exception(curl_error($curl));
                    }
                    curl_close($curl);
                    $variantData = json_decode($variantData, true);

                    if (isset($variantData['errors'])) {
                        throw new Exception('Could not find variant in Walmart database.');
                    }
                } catch (Exception $e) {
                    $errors[] = $e->getMessage();
                }

                $productInfo['variants'][$i]['title'] = isset($variantData['name']) ? $variantData['name'] : '';

                if (isset($variantData['stock']) && $variantData['stock'] == 'Available') {
                    $productInfo['variants'][$i]['in_stock'] = true;
                }

                if (isset($variantData['salePrice'])) {
                    $productInfo['variants'][$i]['source_price'] = floatval($variantData['salePrice']);
                }

                if (isset($variantData['standardShipRate'])) {
                    $productInfo['variants'][$i]['source_price'] += floatval($variantData['standardShipRate']);
                    $productInfo['variants'][$i]['shipping_price'] = floatval($variantData['standardShipRate']);
                }
                $price = number_format($productInfo['variants'][$i]['source_price'],
                    2, '.', '');

                $productInfo['variants'][$i]['source_price'] = $this->calculateCurrency($from_currency, $to_currency, $price);
                $productInfo['variants'][$i]['shopify_price'] = $productInfo['variants'][$i]['source_price'];

                if (isset($variantData['isTwoDayShippingEligible']) && $variantData['isTwoDayShippingEligible'] == true) {
                    $productInfo['variants'][$i]['shipping_type'] = "Two days shipping";
                } else {
                    if (isset($variantData['ninetySevenCentShipping']) && $variantData['ninetySevenCentShipping'] == true) {
                        $productInfo['variants'][$i]['shipping_type'] = "Ninetyseven cent shipping";
                    }
                }

                // Product images
                if (isset($variantData['imageEntities'])) {

                    for ($j = 0; $j < count($variantData['imageEntities']); $j++) {
                        $productInfo['images'][] = [
                            'src' => $variantData['imageEntities'][$j]['largeImage'], 'variant' => $i,
                            'selected' => true
                        ];
                    }

                    $productInfo['variants'][$i]['main_image'] = count($productInfo['images']) - $j + 1;
                }

                // Attributes
                $productInfo['variants'][$i]['attributes'] = [];
                foreach ($variantData['attributes'] as $key => $value) {
                    if (in_array($key, $variantAttributes)) {
                        $productInfo['variants'][$i]['attributes'][] = [
                            'dimension' => ucfirst($key), 'value' => ucfirst($value)
                        ];
                    }
                }
            }
        }
        // remove duplicate images
        foreach ($productInfo['images'] as $key => $val) {
            $srcs[] = $val['src'];
        }

        if( !empty($srcs) ){
            $u_images = array_unique($srcs);
            foreach ($u_images as $key => $val) {
                $productInfo['unique_images'][] = ['src' => $val, 'selected' => true];
            }
        }
        // group by first attributes
        if( !empty( $productInfo['variants'] ) ){
            $productInfo['variants'] = $this->group_by($productInfo['variants']);
        }
        return ['errors' => $errors, 'diagnostics' => $diagnostics, 'product' => $productInfo];
    }

    /**
     * Get a list of collections for the current shop
     *
     * @return \Illuminate\Http\Response
     */
    public function getCollections(Request $request)
    {
        try {
            $shop = Auth::user();

            $errors = [];

            $collections = [];

            $apiRequest = $shop->api()->rest('GET', '/admin/custom_collections.json', ['limit' => 250]);

            if (isset($apiRequest->body->custom_collections)) {
                $collections = $apiRequest->body->custom_collections;
            }

            //fetch associate details
            $res = DB::table('amazon_associates')->select('locale', 'associate_id')->where('user_id', $shop->id)->get();
            $associates = $res->map(function ($name) {
                return $name->locale;
            })->toArray();

            // get counters detaild, and add counter if new plan activated
            $charge = DB::table('charges')->where('status', 'ACTIVE')->where('user_id', $shop->id)->first();
            $counter = Counters::where('user_id', $shop->id)->where('status', 'active')->where('charge_id',
                $charge->id)->first();
            if (!$counter) {
                $last_plan = Counters::where('user_id', $shop->id)->where('status', 'active')->first();
                if($last_plan){
                    $last_plan_id = $last_plan->plan_id;
                    $last_plan->status = 'canceled';
                    $last_plan->save();
                }else{
                    $last_plan_id = 1;
                }

                $counter = new Counters;
                $counter->user_id = $shop->id;
                $counter->charge_id = $charge->id;
                $counter->plan_id = $shop->plan_id;

                if ($last_plan_id == 1) {
                    $counter->regular_product_count = 0;
                    $counter->regular_product_variant_count = 0;
                    $counter->affiliate_product_count = 0;
                    $counter->affiliate_product_variant_count = 0;
                } else {
                    if($last_plan){
                        if( date('Y-m-d H:i:s') > $last_plan->end_date){
                            $counter->regular_product_count = 0;
                            $counter->regular_product_variant_count = 0;
                            $counter->affiliate_product_count = 0;
                            $counter->affiliate_product_variant_count = 0;
                        }else{
                            $counter->regular_product_count = $last_plan->regular_product_count;
                            $counter->regular_product_variant_count = $last_plan->regular_product_variant_count;
                            $counter->affiliate_product_count = $last_plan->affiliate_product_count;
                            $counter->affiliate_product_variant_count = $last_plan->affiliate_product_variant_count;
                        }
                    }
                }
                $counter->status = 'active';
                $counter->is_disable_freemium = ($shop->plan_id == 1) ? 0 : 1;
                $counter->start_date = date('Y-m-d H:i:s');
                $counter->end_date = date('Y-m-d H:i:s', strtotime($counter->start_date.' + 30 days'));
                $counter->save();
            }
            $plan_detail = DB::table('plans')->where('id', $shop->plan_id)->first();
            $curr_plan = $shop->plan_id;
            $plan['affiliate_pr_err_msg'] = '';
            $plan['total_products'] = $counter->regular_product_count;
            $plan['total_affiliate_products'] = $counter->affiliate_product_count;
            $plan['current_plan_name'] = $plan_detail->name;
            $plan['current_plan_id'] = $plan_detail->id;
            if ($plan_detail->max_affiliate_product_import != 'unlimited') {
                $plan['affiliate_pr_err_msg'] = ($plan_detail->max_affiliate_product_import != 'unlimited' && $counter->affiliate_product_count >= $plan_detail->max_affiliate_product_import) ? 'With the '. $plan_detail->name .' plan you can import maximum ' . $plan_detail->max_affiliate_product_import . ' affiliate products, for more import <a href="/settings">please upgrade your plan</a>.' : '';
            }
            $plan['regular_pr_err_msg'] = ($counter->regular_product_count) >= $plan_detail->max_regular_product_import ? 'With the '. $plan_detail->name .' plan you can import maximum ' . $plan_detail->max_regular_product_import . ' regular products, for more import <a href="/settings">please upgrade your plan</a>.' : '';
            if( $curr_plan == 5 ){
                $plan['regular_pr_err_msg'] = ($counter->regular_product_count) >= $plan_detail->max_regular_product_import ? 'With the '. $plan_detail->name .' plan you can import maximum ' . $plan_detail->max_regular_product_import . ' regular products.' : '';
            }

            return [
                'success' => true, 'errors' => $errors, 'collections' => $collections, 'associates' => $associates,
                'plan' => $plan
            ];
        }catch (\Exception $e){
           dump($e);
        }
    }

    /**
     * Get a list of products
     *
     * @return \Illuminate\Http\Response
     */
    public function getProducts(Request $request)
    {
        $shop = Auth::user();

        $errors = [];

        try {
            $products = DB::table('products')->get();

        } catch (QueryException $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }


        return ['success' => true, 'errors' => $errors, 'products' => $products];
    }

    /**
     * Add new product
     *
     * @return \Illuminate\Http\Response
     */
    public function addProduct(Request $request)
    {
        $shop = Auth::user();

        $errors = [];

        // TODO: Check if product import limit has been reached (billing)

        // Get product information in JSON format
        $productInfo = json_decode($request->post('product_info'));

        // ob_start();
        // var_dump($productInfo);
        // $result = ob_get_clean();

        DB::beginTransaction();
        try {
            $affiliate = json_decode($request->post('affiliate'));
            if ($affiliate->status) {
                $res = DB::table('amazon_associates')->select('locale', 'associate_id')->where('user_id',
                    $shop->id)->get();
                $new = $res->map(function ($name) {
                    return $name->locale;
                })->toArray();
                $country = ($affiliate->country == 'COM') ? 'US' : $affiliate->country;
                $country = ($affiliate->country == 'UK') ? 'GB' : $country;
                if (in_array($country, $new)) {
                    $res = DB::table('amazon_associates')->select('associate_id')->where('user_id',
                        $shop->id)->where('locale', $country)->first();
                    if (strpos($productInfo->source_url, '?') !== false) {
                        $productInfo->source_url .= '&tag='.$res->associate_id;
                    } else {
                        $productInfo->source_url .= '?tag='.$res->associate_id;
                    }
                }
            }
            // Initialize Shopify product
            $shopifyProduct = [
                'title' => $productInfo->title,
                'body_html' => $productInfo->description,
                'metafields' => [
                    [
                        'key' => 'amazonedropship',
                        'value' => 'byamazone',
                        'value_type' => 'string',
                        'namespace' => 'dropshipper'
                    ]
                ]
            ];

            if ($productInfo->type) {
                $shopifyProduct['product_type'] = $productInfo->type;
            }

            if ($productInfo->collection) {
                $shopifyProduct['collection'] = $productInfo->collection;
            }


            // Save product
            $productRec = [
                'user_id' => $shop->id,
                'collection_id' => $productInfo->collection,
                'type' => $productInfo->type,
                'locale' => $productInfo->locale,
                'source' => $productInfo->source,
                'source_url' => $productInfo->source_url,
                'description' => $productInfo->description,
                'affiliate' => $request->post('affiliate'),
                'saller_ranks' => json_encode($productInfo->saller_ranks),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ];

            $productID = DB::table('products')->insertGetId($productRec);

            if (!$productID) {
                throw new \Exception('Failed to create product record.');
            }

            // Save images
            $mainProductImageID = null;
            $imageIDArray = [];
            $imageSrcArray = [];
            $shopifyImages = [];
            $variantMainImageURLs = ['']; // Initialize to empty URL for index integrity
            if ($productInfo->unique_images) {
                for ($i = 0; $i < count($productInfo->unique_images); $i++) {

                    // TODO: Add logic to make sure if variant image and !selected then still save
                    if ((!$productInfo->unique_images[$i]->selected && $i != $productInfo->main_image)) {
                        $imageIDArray[] = 0;
                        break;
                    }

                    $imageRec = [
                        'product_id' => $productID,
                        'url' => $productInfo->unique_images[$i]->src,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ];

                    $imageID = DB::table('images')->insertGetId($imageRec);

                    if (!$imageID) {
                        throw new \Exception('Failed to create image record for '.$productInfo->unique_images[$i]->src.'.');
                    }

                    $imageIDArray[] = $imageID;
                    $imageSrcArray[$productInfo->unique_images[$i]->src] = $imageID;
                    if ($i == $productInfo->main_image) {
                        $mainProductImageID = $imageID;
                        $shopifyImages[] = ['src' => $productInfo->unique_images[$i]->src, 'position' => 1];
                        $variantMainImageURLs[0] = $productInfo->unique_images[$i]->src;
                    } else {
                        $shopifyImages[] = ['src' => $productInfo->unique_images[$i]->src];
                    }
                }
            }
//            if ($productInfo->images) {
//
//                for ($i = 0; $i < count($productInfo->images); $i++) {
//
//                    // TODO: Add logic to make sure if variant image and !selected then still save
//                    if ((!$productInfo->images[$i]->selected && $i != $productInfo->main_image)) {
//                        $imageIDArray[] = 0;
//                        break;
//                    }
//
//                    $imageRec = [
//                        'product_id' => $productID,
//                        'url' => $productInfo->images[$i]->src,
//                        'created_at' => date("Y-m-d H:i:s"),
//                        'updated_at' => date("Y-m-d H:i:s")
//                    ];
//
//                    $imageID = DB::table('images')->insertGetId($imageRec);
//
//                    if (!$imageID) {
//                        throw new \Exception('Failed to create image record for '.$productInfo->images[$i]->src.'.');
//                    }
//
//                    $imageIDArray[] = $imageID;
//
//                    if ($i == $productInfo->main_image) {
//                        $mainProductImageID = $imageID;
//                        $shopifyImages[] = ['src' => $productInfo->images[$i]->src, 'position' => 1];
//                        $variantMainImageURLs[0] = $productInfo->images[$i]->src;
//                    } else {
//                        $shopifyImages[] = ['src' => $productInfo->images[$i]->src];
//                    }
//                }
//            }

            // Add images to Shopify product
            if ($shopifyImages) {
                // add maximum 35 image in shopify while create product
                $shopifyProduct['images'] = ( count($shopifyImages) > 35 ) ? array_slice($shopifyImages, 0, 34) : $shopifyImages;
            }

            // Save tags
            $tags = trim($productInfo->tags);
            $shopifyTags = [];

            if ($tags != '') {

                $tagsArray = explode(',', $tags);

                foreach ($tagsArray as $tag) {

                    $tag = trim($tag);

                    $shopifyTags[] = $tag;

                    DB::table('tags')->insert(['product_id' => $productID, 'tag' => $tag]);
                }

                // Add tags to shopify product
                $shopifyProduct['tags'] = $shopifyTags;
            }
            // Construct variant records
            $variantRecs = [];
            $variantAttsRecs = [];
            $shopifyVariants = [];

            // Add main variant
            $variantRecs[] = [
                'product_id' => $productID,
                'image_id' => $mainProductImageID,
                'is_main' => true,
                'title' => $productInfo->title,
                'in_stock' => $productInfo->in_stock,
                'inventory' => $productInfo->inventory,
                'prime_eligible' => $productInfo->prime_eligible,
                'shipping_type' => $productInfo->shipping_type,
                'source_id' => $productInfo->source_id,
                'source_price' => floatval($productInfo->source_price),
                'shipping_price' => floatval($productInfo->shipping_price),
                'shopify_price' => floatval($productInfo->shopify_price),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ];
            // Add main variant to Shopify product
            $mainShopifyVariant = [
                'title' => $productInfo->title,
                'sku' => $productInfo->source_id,
                'price' => floatval($productInfo->shopify_price),
                'inventory_quantity' => $productInfo->inventory,
                'inventory_management' => 'shopify',
                'inventory_policy' => 'deny'
            ];

            $attsArray = [];

            if ($productInfo->attributes) {

                // Set shopify options
                $shopifyOptions = [];

                $shopifyOptNum = 0;

                foreach ($productInfo->attributes as $attr) {
                    $attsArray[] = ['dimension' => $attr->dimension, 'value' => $attr->value];
                    $shopifyOptions[] = ['name' => $attr->dimension];
                    $mainShopifyVariant['option'.++$shopifyOptNum] = $attr->value;
                }

                $shopifyProduct['options'] = $shopifyOptions;
            }
            $shopifyVariants[] = $mainShopifyVariant;

            $variantAttsRecs[] = $attsArray;
            // Add other variants
            if ($productInfo->variants) {

                for ($v = 0; $v < count($productInfo->variants); $v++) {

                    if ($productInfo->variants[$v]->checked) {

                        $mainImageID = null;
//                        if ($productInfo->variants[$v]->main_image && isset($imageIDArray[$productInfo->variants[$v]->main_image])) {
//                            $mainImageID = $imageIDArray[$productInfo->variants[$v]->main_image];
//                            $variantMainImageURLs[] = $productInfo->images[$productInfo->variants[$v]->main_image]->src;
//                        } else {
//                            $variantMainImageURLs[] = $variantMainImageURLs[0]; // use main product image as default variant image
//                        }
//                        if ($productInfo->variants[$v]->main_image && isset($imageIDArray[$productInfo->variants[$v]->main_image])) {
//                            fetch image id from db
                        $main_src_from_variant = $productInfo->images[$productInfo->variants[$v]->main_image]->src;
//                            $mainImageID = $imageIDArray[$productInfo->variants[$v]->main_image];
                        $mainImageID = $imageSrcArray[$main_src_from_variant];

                        if ($productInfo->variants[$v]->main_image) {

                            $variantMainImageURLs[] = $productInfo->images[$productInfo->variants[$v]->main_image]->src;
                        } else {
                            $variantMainImageURLs[] = $variantMainImageURLs[0]; // use main product image as default variant image
                        }
                        $variantRecs[] = [
                            'product_id' => $productID,
                            'image_id' => $mainImageID,
                            'is_main' => false,
                            'title' => $productInfo->variants[$v]->title,
                            'in_stock' => $productInfo->variants[$v]->in_stock,
                            'inventory' => $productInfo->variants[$v]->inventory,
                            'prime_eligible' => $productInfo->prime_eligible,
                            'shipping_type' => $productInfo->shipping_type,
                            'source_id' => $productInfo->variants[$v]->source_id,
                            'source_price' => floatval($productInfo->variants[$v]->source_price),
                            'shipping_price' => floatval($productInfo->variants[$v]->shipping_price),
                            'shopify_price' => floatval($productInfo->variants[$v]->shopify_price),
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s")
                        ];

                        $shopifyVariant = [
                            'title' => $productInfo->variants[$v]->title,
                            'sku' => $productInfo->variants[$v]->source_id,
                            'price' => floatval($productInfo->variants[$v]->shopify_price),
                            'inventory_quantity' => $productInfo->variants[$v]->inventory,
                            'inventory_management' => 'shopify',
                            'inventory_policy' => 'deny'
                        ];

                        $attsArray = [];

                        if ($productInfo->variants[$v]->attributes) {

                            $shopifyOptNum = 0;

                            foreach ($productInfo->variants[$v]->attributes as $attr) {
                                $attsArray[] = ['dimension' => $attr->dimension, 'value' => $attr->value];
                                $shopifyVariant['option'.++$shopifyOptNum] = $attr->value;
                            }
                        }

                        $variantAttsRecs[] = $attsArray;
                        $shopifyVariants[] = $shopifyVariant;
                    }
                }
            }


            // make unique variants
            $option = [];
            $newVariation = [];
            foreach ( $shopifyVariants as $key=>$val ){
                $opt = ( @$val['option1'] ) ? $val['option1'] : '';
                $opt = ( @$val['option2'] ) ? $opt.$val['option2'] : $opt;
                $opt = ( @$val['option3'] ) ? $opt.$val['option3'] : $opt;
                if( !in_array( $opt, $option )){
                    $newVariation[] = $val;
                }
                $option[] = $opt;
            }

//            $shopifyProduct['variants'] = $shopifyVariants;
            $shopifyProduct['variants'] = $newVariation;

            for ($v = 0; $v < count($variantRecs); $v++) {

                $variantID = DB::table('variants')->insertGetId($variantRecs[$v]);

                if (!$variantID) {
                    throw new \Exception('Failed to create variant record (index '.$v.').');
                }

                // Save attributes
                foreach ($variantAttsRecs[$v] as $attr) {
                    DB::table('attributes')->insert([
                        'variant_id' => $variantID, 'dimension' => $attr['dimension'], 'value' => $attr['value']
                    ]);
                }
            }
            // Push product to Shopify
            $productReq = $shop->api()->rest('POST', '/admin/products.json', ['product' => $shopifyProduct]);

            if ($productReq->errors) {
                throw new \Exception('Shopify API error: '.json_encode((array) $productReq->body));
            }

            if (!$productReq->body->product->id) {
                throw new \Exception('Could not retreieve product id from Shopify API');
            }
            $shopifyProductID = $productReq->body->product->id;
            $shopifyProductHandle = $productReq->body->product->handle;

            // Add Shopify ID to product record
            DB::table('products')
                ->where('id', $productID)
                ->update(['shopify_id' => $shopifyProductID, 'shopify_handle' => $shopifyProductHandle]);

            // Add product to collection
            if ($productInfo->collection) {
                $collectReq = $shop->api()->rest('POST', '/admin/collects.json', [
                    'collect' => [
                        'product_id' => $shopifyProductID, 'collection_id' => $productInfo->collection
                    ]
                ]);
            }
            // Assign images to product variants in Shopify
            if ($productReq->body->product->images && $productReq->body->product->variants) {

                $apiImages = $productReq->body->product->images;
                $apiVariants = $productReq->body->product->variants;

                for ($v = 0; $v < count($apiVariants); $v++) {

                    // Locate image index for current variant
                    for ($i = 0; $i < count($shopifyImages); $i++) {
                        if ($variantMainImageURLs[$v] == $shopifyImages[$i]['src']) {
                            $foundImageIndex = $i;
                            break;
                        }
                    }

                    // Set image id for this variant
                    if (isset($apiImages[$foundImageIndex])) {

                        $modVariant = [
                            'id' => $apiVariants[$v]->id,
                            'image_id' => $apiImages[$foundImageIndex]->id
                        ];

                        $variantReq = $shop->api()->rest('PUT', '/admin/variants/'.$apiVariants[$v]->id.'.json',
                            ['variant' => $modVariant]);
                    }
                }
            }

            // increase product import counter
            $charge = DB::table('charges')->where('user_id',$shop->id)->where('status',"ACTIVE")->first();
            if( $charge ){
                $charge_id = $charge->id;
                $counter = Counters::where('user_id',$shop->id)->where('charge_id', $charge_id)->where('status', 'active')->first();

                $affiliate = json_decode($request->post('affiliate'));
                if ($affiliate->status) {
                    $counter->affiliate_product_count = $counter->affiliate_product_count + 1;
                    $counter->affiliate_product_variant_count = $counter->affiliate_product_variant_count + count($productInfo->variants);
                }else{
                    $counter->regular_product_count = $counter->regular_product_count + 1;
                    $counter->regular_product_variant_count = $counter->regular_product_variant_count + count($productInfo->variants);
                }
                $counter->save();
            }
            //throw new \Exception(json_encode((array) $productReq));

            // add remaining product images in product
            $jobmsg = false;
            if( ( count($shopifyImages) > 35 ) ){
                $remaining_imges = array_slice($shopifyImages, 35);
                AddProductImagesJob::dispatch($shop->id, $remaining_imges, $shopifyProductID);
                $jobmsg = true;
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }

        DB::commit();
        $product['view'] = 'https://'.$shop->name.'/products/'.$shopifyProductHandle;
        $product['edit'] = 'https://'.$shop->name.'/admin/products/'.$shopifyProductID;
        $product['my_product'] = '/products';
        $product['jobmsg'] = $jobmsg;
        return ['success' => true, 'errors' => $errors, 'data' => $product];
    }
}
