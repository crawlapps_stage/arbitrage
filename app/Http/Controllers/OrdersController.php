<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Show Orders page
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('orders');
    }
}
