<?php

namespace App\Http\Controllers;

use App\Models\Counters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Symfony\Component\Intl\Currencies;

class ProductsController extends Controller
{
    /**
     * Show Products page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shop = Auth::user();
        $charge = DB::table('charges')->where('user_id',$shop->id)->where('status',"ACTIVE")->first();
        if( $charge ){
            $counters = Counters::where('user_id', $shop->id)->where('charge_id', $charge->id)->first();
            $plan = DB::table('plans')->where('id', $shop->plan_id)->first();
            $counter['current_plan'] = $plan->name;
            $counter['total_product'] = ($counters) ? $counters->regular_product_count : 0;
            $counter['total_affiliate_product'] = ($counters) ? $counters->affiliate_product_count : 0;
        }

        return view('products', compact('counter'));
    }

    /**
     * Show Products page
     *
     * @return \Illuminate\Http\Response
     */
    public function getProducts()
    {
        $shop = Auth::user();

        $shopURL = 'https://' . $shop->getDomain()->toNative();

        $errors = [];

        try {
            $productRecs = DB::table('products')
                ->join('variants', 'products.id', '=', 'variants.product_id')
                ->join('images', 'variants.image_id', '=', 'images.id')
                ->where('variants.is_main', true)
                ->where('user_id', $shop->id)
                ->select('products.*', 'variants.title', 'variants.in_stock',  'variants.inventory',
                         'variants.prime_eligible', 'variants.shipping_type', 'variants.source_id','variants.source_price_sign',
                         'variants.source_price', 'variants.shopify_price')
                ->orderBy('products.updated_at', 'desc')
                ->get();

            //get attribues
            $products = DB::table('products')->get();
            foreach( $products as $key=>$val ){
                $variants = DB::table('variants')->select('id', 'source_id')->where('product_id', $val->id)->get();
                foreach ( $variants as $vk=>$vv ){
                    $attributes = DB::table('attributes')->where('variant_id', $vv->id)->get();
                    if( $attributes->count() > 0 ) {
                        foreach ( $attributes as $ak=>$av ){
                                $attributesFinal[$val->shopify_id][$vv->source_id][$av->dimension] = $av->value;
                                $attributesFinal[$val->shopify_id]['key'][] = $av->dimension;
                                $attributesFinal[$val->shopify_id]['key'] = array_unique($attributesFinal[$val->shopify_id]['key']);

                        }
                    }else{
                        $attributesFinal[$val->shopify_id][$vv->source_id] = [];
                        $attributesFinal[$val->shopify_id]['key'] = [];
                        $attributesFinal[$val->shopify_id]['key'] = [];
                    }
                }
            }

//            $attributesRecs = DB::table('products')
//                ->join('variants', 'products.id', '=', 'variants.product_id')
//                ->join('attributes', 'variants.id','=', 'attributes.variant_id')
//                ->select( 'products.shopify_id','attributes.*', 'variants.source_id')
//                ->get();

//            foreach ( $attributesRecs as $key=>$val ){
//                $attributesFinal[$val->shopify_id][$val->source_id][$val->dimension] = $val->value;
//                $attributesFinal[$val->shopify_id]['key'][] = $val->dimension;
//                $attributesFinal[$val->shopify_id]['key'] = array_unique($attributesFinal[$val->shopify_id]['key']);
//            }
        } catch (QueryException $e) {
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }

        $shoppifyIDArray = [];
        $products = [];

        for ($i = 0; $i < count($productRecs); $i++) {
            $shoppifyIDArray[] = $productRecs[$i]->shopify_id;
        }

        $productReq = $shop->api()->rest('GET', '/admin/products.json', ['ids' => implode(',', $shoppifyIDArray)]);

        if ($productReq->body->products) {

            $shopifyProducts = $productReq->body->products;

            for ($p = 0; $p < count($productRecs); $p++) {
                $shopifyRec = [];
                for ($s = 0; $s < count($shopifyProducts); $s++ ) {
                    if ($shopifyProducts[$s]->id == $productRecs[$p]->shopify_id ) {
                        $shopifyRec = $shopifyProducts[$s];
                        break;
                    }
                }

                if( !empty($shopifyRec) ) {
                    // Get product variants
                    $variantRecs = DB::table('variants')->where('product_id', $productRecs[$p]->id)->get();

                    $updatedAt = date('F/j/Y h:i A', strtotime($productRecs[$p]->updated_at));

                    $products[$p] = [
                        'id' => $productRecs[$p]->id,
                        'shopify_id' => $shopifyRec->id,
                        'title' => $productRecs[$p]->title,
                        'source' => $productRecs[$p]->source,
                        'source_url' => $productRecs[$p]->source_url,
                        'locale' => $productRecs[$p]->locale,
                        'shipping_type' => $productRecs[$p]->shipping_type,
                        'associates' => json_decode($productRecs[$p]->affiliate),
                        'updated_at' => $updatedAt,
                        'options' => $shopifyRec->options,
                        'source_prices' => [],
                        'variants' => $shopifyRec->variants,
                        'images' => $shopifyRec->images,
                        'image_url' => '',
                        'product_url' => $shopURL.'/products/'.$shopifyRec->handle,
                        'edit_url' => $shopURL.'/admin/products/'.$shopifyRec->id,
                        'min_source_price' => $variantRecs[0]->source_price,
                        'max_source_price' => $variantRecs[0]->source_price,
                        'min_price' => $shopifyRec->variants[0]->price,
                        'max_price' => $shopifyRec->variants[0]->price,
                        'saller_ranks' => json_decode($productRecs[$p]->saller_ranks),
                    ];

                    if ($productRecs[$p]->source == 'Amazon' && $productRecs[$p]->prime_eligible) {
                        $products[$p]['shipping_type'] = 'Prime - 3 Days';
                    }

                    if ($shopifyRec->images) {
                        $products[$p]['image_url'] = $shopifyRec->images[0]->src;
                    }

                    for ($v = 0; $v < count($shopifyRec->variants); $v++) {
                        $products[$p]['min_price'] = min($products[$p]['min_price'], $shopifyRec->variants[$v]->price);
                        $products[$p]['max_price'] = max($products[$p]['max_price'], $shopifyRec->variants[$v]->price);

                        foreach ($variantRecs as $vRec) {
                            if ($shopifyRec->variants[$v]->sku == $vRec->source_id) {
                                $products[$p]['source_price_sign'] = $vRec->source_price_sign;
                                $products[$p]['source_prices'][] = $vRec->source_price;
                                $products[$p]['min_source_price'] = min($products[$p]['min_source_price'],
                                    $vRec->source_price);
                                $products[$p]['max_source_price'] = max($products[$p]['max_source_price'],
                                    $vRec->source_price);
                                break;
                            }
                        }
                    }
                }
            }
        }

        // fetch native store currency
        $shop = Auth::user();
        $parameter['fields'] = 'currency';
        $shop_result = $shop->api()->rest('GET', 'admin/api/' . env('SHOPIFY_API_VERSION') . '/shop.json', $parameter);
        if( !$shop_result->errors ){
            $currency = Currencies::getSymbol($shop_result->body->shop->currency);
        }

        return ['success' => true, 'errors' => $errors, 'products' => $products, 'attributes' => $attributesFinal, 'currency'=> $currency];
    }

    /**
     * Get product variants
     *
     * @return \Illuminate\Http\Response
     */
    public function getVariants()
    {

    }

    /**
     * Update product variants
     *
     * @return \Illuminate\Http\Response
     */
    public function saveVariants(Request $request)
    {
        $shop = Auth::user();

        $errors = [];

        $productID = $request->post('product_id');

        if (!preg_match('~^\d+$~', $productID)) {
            return [ 'success' => false, 'errors' => ["Invalid product id: $productID"]];
        }

        $productRec = DB::table('products')->where('id', $productID)->first();

        if (!$productRec) {
            return [ 'success' => false, 'errors' => ["Product id $productID was not found in the database."]];
        }

        $variants = json_decode($request->post('variants'));

        DB::beginTransaction();

        try {
            foreach ($variants as $variant) {

                // Update database record
                DB::table('variants')
                    ->where([
                        ['product_id', '=', $productID],
                        ['source_id', '=', $variant->sku]
                    ])
                    ->update(array('shopify_price' => floatval($variant->price)));

                // Update Shopify variant
                $modVariant = [
                    'id' => $variant->id,
                    'price' => $variant->price
                ];

                $variantReq = $shop->api()->rest('PUT', '/admin/variants/' . $variant->id . '.json', ['variant' => $modVariant]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }

        DB::commit();

        return ['success' => true, 'errors' => $errors];
    }

    /**
     * Delete product
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteProduct(Request $request)
    {
        $shop = Auth::user();

        $errors = [];

        $productID = $request->post('product_id');

        if (!preg_match('~^\d+$~', $productID)) {
            return [ 'success' => false, 'errors' => ["Invalid product id: $productID"]];
        }

        $productRec = DB::table('products')->where('id', $productID)->first();

        if (!$productRec) {
            return [ 'success' => false, 'errors' => ["Product id $productID was not found in the database."]];
        }

        DB::beginTransaction();

        try {
            $variantRecs = DB::table('variants')->where('product_id', $productID)->get();

            // Delete attributes
            foreach ($variantRecs as $vRec) {
                DB::table('attributes')->where('variant_id', $vRec->id)->delete();
            }

            // Delete variants
            DB::table('variants')->where('product_id', $productID)->delete();

            // Delete images
            DB::table('images')->where('product_id', $productID)->delete();

            // Delete tags
            DB::table('tags')->where('product_id', $productID)->delete();

            // Delete product rec
            DB::table('products')->where('id', $productID)->delete();

            // Delete product from Shopify
            // TODO: Verify deletion, throw exception if unsuccessful
            if ($productRec->shopify_id) {
                $productReq = $shop->api()->rest('DELETE', '/admin/products/' . $productRec->shopify_id . '.json');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            $errors[] = $e->getMessage();
            return ['success' => false, 'errors' => $errors];
        }

        DB::commit();

        return ['success' => true, 'errors' => $errors];
    }
}
