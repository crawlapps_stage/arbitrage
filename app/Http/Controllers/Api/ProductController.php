<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Response;

class ProductController extends Controller
{
   public  function checkAffiliate(Request $request){
       try{
           $shop_domain = $request->shop;
           if ($shop_domain) {
               $user = User::where('name', $shop_domain)->first();
               $product = Product::where('user_id', $user->id)->where('shopify_handle', $request->handle)->first();
               if( $product ){
                   $data['is_product'] = true;
                   $buttonstyle = json_decode($product->affiliate);
                   if( $buttonstyle->status ){
                       $data['is_product'] = true;
                       $data['redirect'] = $product->source_url;
                       $data['show_data'] = $buttonstyle->button_text;
                       $data['style'] = $buttonstyle->button_style;
                   }else{
                       $data['is_product'] = false;
                   }
               }else{
                   $data['is_product'] = false;
               }
               return Response::json(["data" => $data], 200);
           }
       }catch (\Exception $e){
           return Response::json(["data" => $e], 422);
       }
   }
}
