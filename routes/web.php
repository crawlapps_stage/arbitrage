<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth.shopify','billable']], function () {
    Route::get('/', 'AppController@import')->name('home');

    Route::get('/products', 'ProductsController@index');
    Route::post('/products/get-products', 'ProductsController@getProducts');
    Route::post('/products/save-variants', 'ProductsController@saveVariants');
    Route::post('/products/delete-product', 'ProductsController@deleteProduct');

    Route::post('/get-amazon-product', 'AppController@getAmazonProduct');
    Route::post('/get-walmart-product', 'AppController@getWalmartProduct');
    Route::post('/get-collections', 'AppController@getCollections');
    Route::post('/add-product', 'AppController@addProduct');

    Route::get('/orders', 'OrdersController@index');

    Route::get('/settings', 'SettingsController@index');
    Route::post('/settings/set-plan', 'SettingsController@setPlan');

    Route::get('/settings/get-amazon-associates', 'SettingsController@getAmazonAssociates');
    Route::post('/settings/add-amazon-associate', 'SettingsController@addAmazonAssociate');
    Route::post('/settings/delete-amazon-associate', 'SettingsController@deleteAmazonAssociate');

    Route::post('/settings/set-amazon-associate-btn', 'SettingsController@setAmazonAssociateBtn');
    Route::post('/settings/set-advanced', 'SettingsController@setAdvanced');

    Route::get('/help', 'HelpController@index');
    Route::get('/help/tutorials', 'HelpController@tutorials');
    Route::get('/help/contact', 'HelpController@contact');
    Route::get('/help/announcements', 'HelpController@announcements');

    //Route::get('/notifications', 'AppController@notifications')->middleware(['auth.shopify']);
});

Route::get('flush', function(){
    request()->session()->flush();
});
