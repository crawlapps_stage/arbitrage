<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => ['cors'], 'namespace' => 'Api'], function () {
    Route::get('check-affiliate', 'ProductController@checkAffiliate');
});

//Route::post('/get-amazon-product', 'AppController@getAmazonProduct');
//Route::post('/get-walmart-product', 'AppController@getWalmartProduct');
//Route::post('/get-collections', 'AppController@getCollections');
//Route::post('/add-product', 'AppController@addProduct')->middleware(['auth.shopify']);


